<?php
class HomeController
{
    public \HomeModel $model;

    public function indexAction()
    {
        unset($_SESSION["error_msg"]);
        $this->checkUserAuth();
    }

    protected function checkUserAuth()
    {


        if (!empty($_SESSION["user_id"])) {
            return require_once('../app/views/DashBoard.php');
            exit;
        } else if (!empty($_COOKIE["useremail"]) && !empty($_COOKIE["random_password"])) {
            // fetch token fron db:
            $userToken = $this->model->getTokenByEmail($_COOKIE["useremail"], 0);

            // Authentication token verification
            $isPasswordAndTokenVerified = $this->verifyStoredToken($userToken["password_hash"], $userToken["expiry_date"]);

            // Redirect if cookie based validation retuens true
            // Else, mark the token as expired and clear cookies
            if (!empty($userToken["id"]) && $isPasswordAndTokenVerified) {
                return require_once('../app/views/DashBoard.php');
                exit;
            } else {
                if (!empty($userToken["id"])) {
                    $this->model->markAsExpired($userToken["id"]);
                }
                require_once('../app/utils/auth.utils.php');
                AuthUtils::clearAuthCookie();
                return require_once('../app/views/Login1.php');
                exit;
            }
        } else {
            return require_once('../app/views/Login1.php');
            exit;
        }
    }


    private function verifyStoredToken(string $pwd, string $expiry): bool
    {
        $verified = true;

        // Validate random password cookie with database
        if (!password_verify($_COOKIE["random_password"], $pwd)) {
            $verified = false;
        }

        // check cookie expiration by date
        if ($expiry <= date("Y-m-d H:i:s", time())) {
            $verified = false;
        }

        return $verified;
    }
}
