<?php

class UserController
{
    public UserModel $model;
    private const NAMEREGEX = "/^[a-zA-Z0-9.]*$/";
    private const PASSWORDREGEX = "/^[a-zA-Z0-9]*$/";

    public function loginAction()
    {

        if (isset($_POST['login-submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
            // check for remember me option:
            if (!isset($_POST['remember']) || $_POST['remember'] == false || $_POST['remember'] == null) {
                $this->uncheckRememberMe();
            }

            $response = $this->model->loginUser($email, $password);

            if ($response != false) {
                $_SESSION['userLogedIn'] = 1;
                $_SESSION['username'] = $response["name"];
                $_SESSION['user_id'] = $response["id"];
                $_SESSION['useremail'] = $response["email"];
                // check for remember me option:
                if (isset($_POST['remember'])) {
                    $this->setCookieAndInsertToken($response["email"]);
                }
                return require_once('../app/views/DashBoard.php');
            } else {
                $_SESSION["error_msg"] = 'Wrong credentials!';
                return require_once('../app/views/Login1.php');
            }
        }
        return require_once('../app/views/Login1.php');
    }

    public function logoutAction()
    {
        require_once('../app/utils/auth.utils.php');
        AuthUtils::clearAuthCookie();
        unset($_SESSION['userLogedIn']);
        unset($_SESSION['user_id']);
        unset($_SESSION['useremail']);
        session_unset();
        session_destroy();

        return require_once('../app/views/Login1.php');
    }

    public function registerAction()
    {

        if (isset($_POST['register-submit'])) {
            $email = htmlspecialchars($_POST['email']);
            $username = htmlspecialchars($_POST['name']);
            $password = htmlspecialchars($_POST['password']);

            $this->validateInputs($username, $email, $password);

            if ($this->model->checkIfUserAlredyExistByEmail($email)) {
                // returns true if the given email is already taken                
                $_SESSION["error_msg"] = "Account with emai: $email alredy exists!";
                return require_once('../app/views/Register.php');
                exit();
            }

            $hashedPwd = password_hash($password, PASSWORD_DEFAULT);

            // inputs are valide and email is not taken:
            // perform sign up new user:
            $res = $this->model->signup($email, $hashedPwd, $username);

            if ($res) {
                $_SESSION['userLogedIn'] = 1;
                $_SESSION['username'] = $username;
                // $_SESSION['user_id'] ;
                $_SESSION['useremail'] = $email;
                return require_once('../app/views/DashBoard.php');
            } else {
                $_SESSION["error_msg"] = 'We can not register you';
                return require_once('../app/views/Register.php');
            }
        }
        return require_once('../app/views/Register.php');
    }
    // REMEMBER ME:

    public function setCookieAndInsertToken(string $email)
    {
        require_once('../app/utils/auth.utils.php');

        $cookie_expiration_time = time() + (3600 * 24 * 3);
        setcookie('remember_checked', $_POST['remember'], 0, '/');
        setcookie('useremail', $email, 0, '/');

        $random_password = AuthUtils::getToken(16);
        setcookie('random_password', $random_password, 0, '/');

        $random_password_hash = password_hash($random_password, PASSWORD_DEFAULT);


        $expiry_date = date("Y-m-d H:i:s", $cookie_expiration_time);

        // mark existing token as expired
        $userToken = $this->model->getTokenByEmail($email, 0);
        if (!empty($userToken[0]["id"])) {
            $this->model->markAsExpired($userToken[0]["id"]);
        }
        // Insert new token
        $this->model->insertToken($email, $random_password_hash, $expiry_date);
    }
    public function uncheckRememberMe()
    {
        setcookie('remember_checked', false, -2, '/');
        unset($_POST['remember']);
    }

    // error handling:

    private function validateInputs($username, $userEmail, $userPassword)
    {
        $invalidCredentialMsg = "Email address or password ar incorect! Please type valid email address and valid password";
        if (!$this->verifyName($username)) {
            // echo "invalid username";
            $_SESSION["error_msg"] = "This name: $username is invalid! Please type valid name";
            return require_once('../app/views/Register.php');
            exit();
        }
        if (!$this->verifyEmail($userEmail)) {
            // echo "invalid email";
            $_SESSION["error_msg"] = $invalidCredentialMsg;
            return require_once('../app/views/Register.php');
            exit();
        }
        if (!$this->verifyPassword($userPassword)) {
            // echo "invalid password";
            $_SESSION["error_msg"] = $invalidCredentialMsg;
            return require_once('../app/views/Register.php');
            exit();
        }
    }
    private function verifyName($name)
    {
        if ($this->validInput(UserController::NAMEREGEX, $name, 2)) {
            return true;
        } else {
            return false;
        }
    }
    private function verifyEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
    private function verifyPassword($pwd)
    {
        if ($this->validInput(UserController::PASSWORDREGEX, $pwd, 5)) {
            return true;
        } else {
            return false;
        }
    }
    // input validation
    function validInput(string $matcher, string $input, int $minCharakters)
    {
        if (preg_match($matcher, $input) && strlen($input) >= $minCharakters) {
            return true;
        } else {
            return false;
        }
    }
}
