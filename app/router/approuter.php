<?php

$routeList = [];

class AppRouter
{
    public static function add($routeName, $pointControllerAction)
    {
        $GLOBALS['routeList'][] = ['name' => $routeName, 'action' => $pointControllerAction];
    }
    public static function run()
    {
        $request = $_SERVER['REQUEST_URI'];
        foreach ($GLOBALS['routeList'] as $route) {
            if ($route['name'] == $request) {
                return $route['action'];
            }
        }
    }
}
AppRouter::add("/loginregister1/home", 'HomeController@indexAction');
AppRouter::add("/loginregister1/login", 'UserController@loginAction');
AppRouter::add('/loginregister1/logout', 'UserController@logoutAction');
AppRouter::add("/loginregister1/register", 'UserController@registerAction');

$route = AppRouter::run();
