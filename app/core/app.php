<?php
session_start();
include_once('../app/config/dbconfig.php');
include_once('../app/db/Dbh.php');

spl_autoload_register('myAutoLoader');

function myAutoLoader(string $className)
{
    $path_ctr = '../app/controller/';
    $path_model = "../app/model/";
    $extension = ".php";

    $full_path_ctr = $path_ctr . $className . $extension;
    $full_path_model = $path_model . $className . $extension;

    if (file_exists($full_path_ctr)) {
        require $full_path_ctr;
    }
    if (file_exists($full_path_model)) {
        require $full_path_model;
    }

    if (!file_exists($full_path_ctr) && !file_exists($full_path_model)) {
        return false;
    }
}


$db = Dbh::connect($db_config);

// AppRouter:
include_once(__DIR__ . '../../router/approuter.php');

if (!empty($route)) {
    // in router: $route = "LoginController@indexAction"; -> 
    // [LoginController, indexAction]
    $routes = explode('@', $route);

    $ctr = ucfirst($routes[0]);
    $model1 = ucfirst(str_replace('Controller', 'Model',  $routes[0]));
    $action = lcfirst($routes[1]);
} else {
    // default route
    $ctr = 'HomeController';
    $model1 = 'HomeModel';
    $action = 'indexAction';
}
$load_new = new $ctr();
$model = new $model1();
$model->db = $db;
$load_new->model = $model;
// hardcoded:
$index = $load_new->$action();
