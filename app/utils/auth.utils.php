<?php
// It is not secure to store the plain password in the cookie, the random numbers are 
// generated as the authentication keys.
// These keys are hashed and stored in the database with an expiration period.
// Once the time expires, then the expiration flag will be set to 0

class AuthUtils
{

    public static function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[AuthUtils::cryptoRandSecure(0, $max)];
        }
        return $token;
    }

    private static function cryptoRandSecure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) {
            return $min; // not so random...
        }
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    public static function clearAuthCookie()
    {
        if (isset($_COOKIE["useremail"])) {
            setcookie("useremail", "", time() - 7200, '/');
        }
        if (isset($_COOKIE["random_password"])) {
            setcookie("random_password", "", time() - 7200, '/');
        }
        if (isset($_COOKIE["remember_checked"])) {
            setcookie("remember_checked", "", time() - 7200, '/');
        }
    }
}
