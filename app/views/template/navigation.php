<nav class="navigation">
    <ul class="nav-list">
        <?php
        if (isset($_SESSION['username'])) {
        ?>
            <li id="hello"><a href="#"> Hello <?php echo $_SESSION['username']; ?></a></li>
            <li id="logout"><a href="logout">LOGOUT</a></li>
        <?php
        } else {
        ?>
            <li id="signup"><a href="register">SIGN UP</a></li>
            <li id="login"><a href="login">LOGIN</a></li>
        <?php
        }
        ?>
    </ul>
</nav>