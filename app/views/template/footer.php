<div class="footer-wrapper" style="background-color: rgba(0, 0, 0, 0.2); margin-bottom: 0px; padding-bottom:  10px;">

    <p>Registration and login system with PHP PDO. Remember me with cookie</p>
    <p>
        Hello. <?php if (!isset($_SESSION['username'])) {
                    echo 'You are not signed in.';
                } else {
                    echo "You are signed in as: " . $_SESSION["username"];
                } ?>
    </p>
    <div class="error-message">
        <?php if (isset($_SESSION["error_msg"])) {
            echo $_SESSION["error_msg"];
        } else {
            echo 'No errors found';
        } ?>
    </div>

</div>