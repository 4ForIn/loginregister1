<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

    <link rel="stylesheet" type="text/css" href="public/css/style.css" />

    <title>Login Form - PHPMVC</title>
</head>

<body>
    <header>
        <?php include_once 'template/navigation.php'; ?>
    </header>

    <div class="container">
        <div class="error-message">
            <?php if (isset($_SESSION["error_msg"])) {
                echo $_SESSION["error_msg"];
            } ?>
        </div>
        <form action="login" method="POST" class="login-email">
            <p class="login-text" style="font-size: 2rem; font-weight:800">Login</p>
            <div class="input-group">
                <input type="email" placeholder="Email" name="email" value="<?php if (isset($_COOKIE['useremail'])) {
                                                                                echo $_COOKIE['useremail'];
                                                                            } ?>" required />
            </div>
            <div class="input-group">
                <input type="password" placeholder="Password" name="password" value="<?php if (isset($_COOKIE['password'])) {
                                                                                            echo $_COOKIE['password'];
                                                                                        } ?>" required />
            </div>
            <div class="input-group">
                <button type="submit" class="btn" name="login-submit">Login</button>
            </div>
            <div class="login-remember">
                <input type="checkbox" name="remember" <?php if (isset($_COOKIE['remember_checked'])) {
                                                            echo "checked";
                                                        } ?>> <span> Remeber Me </span>
                <a href="recover.php" class="float-right"> Forgot Password </a>
            </div>
            <p class="login-register-text">Don't have an account? <a href="register">Register here.</a></p>
        </form>

    </div>
    <footer>
        <?php include_once 'template/footer.php';
        ?>
    </footer>
</body>

</html>