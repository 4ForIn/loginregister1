<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

    <link rel="stylesheet" type="text/css" href="public/css/style.css" />

    <title>Register Form</title>
</head>

<body>
    <header>
        <?php include_once 'template/navigation.php'; ?>
    </header>
    <div class="container">
        <div class="error-message">
            <?php if (isset($_SESSION["error_msg"])) {
                echo $_SESSION["error_msg"];
            } ?>
        </div>
        <form action="register" method="POST" class="login-email">
            <p class="login-text" style="font-size: 2rem; font-weight:800">Register</p>
            <div class="input-group">
                <input type="text" placeholder="Name" name="name" required />
            </div>
            <div class="input-group">
                <input type="email" placeholder="Email" name="email" required />
            </div>
            <div class="input-group">
                <input type="password" placeholder="Password" name="password" required />
            </div>
            <div class="input-group">
                <button type="submit" class="btn" name="register-submit">Register new user</button>
            </div>
            <p class="login-register-text">Have an account? <a href="login">Login.</a></p>
        </form>
    </div>
    <footer>
        <?php include_once 'template/footer.php';
        ?>
    </footer>
</body>

</html>