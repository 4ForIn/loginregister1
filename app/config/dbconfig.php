<?php
return $db_config = [
    'host' => 'localhost',
    'dbname' => 'php_registration',
    'username' => 'root',
    'pwd' => '',
    'charset' => 'utf8',
    'options' => [
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ]
];
