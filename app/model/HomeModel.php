<?php
class HomeModel
{
    public $db;


    function getTokenByEmail($email, $expired)
    {

        $query = 'SELECT * FROM tbl_token_auth WHERE email = ? AND is_expired = ?';
        $stmt = $this->db->prepare($query);
        $stmt->execute(array($email, $expired));
        $token_data = $stmt->fetch();
        return $token_data;
    }

    function markAsExpired($tokenId)
    {

        $expired = 1;
        $query = 'UPDATE tbl_token_auth SET is_expired = ? WHERE id = ?';
        $stmt = $this->db->prepare($query);
        $stmt->execute(array($expired, $tokenId));
        return $stmt;
    }
}
