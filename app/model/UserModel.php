<?php
class UserModel
{
    public $db;

    public function loginUser(string $username, string $password)
    {
        unset($_SESSION["error_msg"]);
        $query = 'SELECT * FROM users WHERE email = ?';
        try {
            $stmt = $this->db->prepare($query);
            $stmt->execute(array($username));

            $checkPwd = false;
            if ($stmt->rowCount() > 0) {
                $user_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $checkPwd = password_verify($password, $user_data[0]['password']);
            }

            if ($checkPwd == true) {
                unset($_SESSION["error_msg"]);
                return $user_data[0];
            } else {
                $stmt = null;
                return false;
            }
        } catch (PDOException $e) {
            $_SESSION["error_msg"] = $e->getMessage();
            echo 'loginUser Error: ' . $e->getMessage();
        }
    }
    public function signup(
        string $email,
        string $hashedPwd,
        string $name,
        string $user_type = 'user',
        $info = null
    ): bool {
        unset($_SESSION["error_msg"]);
        if ($this->checkIfUserAlredyExistByEmail($email)) {
            $_SESSION["error_msg"] = "User with email: $email alredy exist.";
            return false;
        } else {
            $res = $this->insertNewUser($email, $hashedPwd, $name, $user_type, $info);
            unset($_SESSION["error_msg"]);
            return $res;
        }
    }

    // return true if the given email is already taken
    public function checkIfUserAlredyExistByEmail($email): bool
    {
        $query_user = "SELECT id FROM users WHERE email = ?";
        // $this->db from Dbh class
        $stmt = $this->db->prepare($query_user);
        // $stmt->bindParam(1, $email, PDO::PARAM_STR);

        if (!$stmt->execute(array($email))) {
            // executing the statment failed
            $stmt = null;
            $_SESSION["error_msg"] = "Sorry connection to database was unavailable. Please try again.";

            exit();
        }
        $resultCheckUser = false;

        // if in database there is user with the given email return true
        if ($stmt->rowCount() > 0) {
            $resultCheckUser = true;
        }

        return $resultCheckUser;
    }

    protected function insertNewUser(
        string $email,
        string $hashedPwd,
        string $name,
        string $user_type = 'user',
        $info = null
    ): bool {
        $date1 = date("Y-m-d-H:i");
        $query = 'INSERT INTO users SET email = :email, password = :password, name = :name, user_type = :user_type, info = :info, created = :created';

        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':password', $hashedPwd, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':user_type', $user_type, PDO::PARAM_STR);
        $stmt->bindParam(':info', $info, PDO::PARAM_STR | PDO::PARAM_NULL);
        $stmt->bindParam(':created', $date1, PDO::PARAM_STR);
        // Execute query
        if ($stmt->execute()) {
            print("user created");
            unset($_SESSION["error_msg"]);
            $stmt = null;
            return true;
        }
    }

    function getTokenByEmail($email, $expired)
    {

        $query = 'SELECT * FROM tbl_token_auth WHERE email = ? AND is_expired = ?';
        $stmt = $this->db->prepare($query);
        $stmt->execute(array($email, $expired));
        $token_data = $stmt->fetch();
        return $token_data;
    }

    function markAsExpired($tokenId)
    {

        $expired = 1;
        $query = 'UPDATE tbl_token_auth SET is_expired = ? WHERE id = ?';
        $stmt = $this->db->prepare($query);
        $stmt->execute(array($expired, $tokenId));
        return $stmt;
    }

    function insertToken($email, $random_password_hash,  $expiry_date)
    {

        $query = "INSERT INTO tbl_token_auth (email, password_hash, expiry_date) VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->execute(array($email, $random_password_hash, $expiry_date));
        return $stmt;
    }
}
