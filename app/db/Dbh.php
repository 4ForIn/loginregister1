<?php
// singleton
class Dbh
{

    public static $conn = false;
    private function __construct()
    {
    }

    // DB Connect
    public static function connect($config)
    {

        if (!self::$conn) {
            try {
                $connection = new PDO(
                    'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'] . ';charset=' . $config['charset'],
                    $config['username'],
                    $config['pwd'],
                    $config['options'],
                );
                $connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
                self::$conn = $connection;
                return self::$conn;
            } catch (\PDOException $e) {
                echo 'Connection Error: ' . $e->getMessage();
                die();
            }
        }

        return self::$conn;
    }
}
